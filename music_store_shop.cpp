#include <iostream>
#include <conio.h>

/*
    Author: M00605010
    Date: 14.01.2021
    Module: CST 2550
*/

int main()
{
    //Variables for Menu selection
    int main_menu, selected_menu;
    // Welcome screen for users
    std::cout << "\n\n\n\n\n\n\n\n\n\n\n \t\t\t |---- MUSIC SHOP ----|";
    getch();       // conio function to go to next screen
    system("cls"); // clear screen
    // Main Menu
    std::cout << "\n\t\t Music Shop Management \n";
    std::cout << "\n\n\t\t 1: Administrator Menu";
    std::cout << "\n\n\t\t 2: Customer Menu";
    std::cout << "\n\n\t\t 3: Exit";
    std::cout << "\n\nSelect a number (1 - 3): ";
    std::cin >> main_menu; // user input

    if (main_menu == 1)
    {
        system("cls"); // clear screen
    }
    return 0;
}